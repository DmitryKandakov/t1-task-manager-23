package ru.t1.dkandakov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.repository.ICommandRepository;
import ru.t1.dkandakov.tm.api.repository.IProjectRepository;
import ru.t1.dkandakov.tm.api.repository.ITaskRepository;
import ru.t1.dkandakov.tm.api.repository.IUserRepository;
import ru.t1.dkandakov.tm.api.service.*;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.command.project.*;
import ru.t1.dkandakov.tm.command.system.*;
import ru.t1.dkandakov.tm.command.task.*;
import ru.t1.dkandakov.tm.command.user.*;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkandakov.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkandakov.tm.model.Project;
import ru.t1.dkandakov.tm.model.User;
import ru.t1.dkandakov.tm.repository.CommandRepository;
import ru.t1.dkandakov.tm.repository.ProjectRepository;
import ru.t1.dkandakov.tm.repository.TaskRepository;
import ru.t1.dkandakov.tm.repository.UserRepository;
import ru.t1.dkandakov.tm.service.*;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @Getter
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationArgumentListCommand());
        registry(new ApplicationCommandListCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationSystemInfoCommand());
        registry(new ApplicationVersionCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowProjectsCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskBindToProjectCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskShowCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLockCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRemoveCommand());
        registry(new UserUnlockCommand());
    }

    public void run(@NotNull String[] args) {
        initLogger();
        initDemoData();
        processArguments(args);
        processCommands();
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@test.ru");
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("BETA PROJECT", Status.COMPLETED));

        taskService.create(test.getId(), "MEGA TASK", "111111");
        taskService.create(test.getId(), "BETA TASK", "222222");
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            @NotNull final String argument = args[0];
            processArgument(argument);
            loggerService.command(argument);
            System.exit(0);
        } catch (@NotNull final Exception e) {
            System.err.println("[FAIL]");
            loggerService.error(e);
            System.exit(1);
        }
    }

    private void processArgument(@NotNull final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                super.run();
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

}