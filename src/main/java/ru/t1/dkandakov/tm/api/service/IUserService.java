package ru.t1.dkandakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(String login, String password);

    @NotNull
    User create(String login, String password, String email);

    @NotNull
    User create(String login, String password, Role role);

    @Nullable
    User findByLogin(String login);

    @Nullable
    User findByEmail(String email);

    @Nullable
    User removeByLogin(String login);

    @Nullable
    User removeByEmail(String email);

    @NotNull
    User setPassword(String id, String password);

    @NotNull
    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

    @NotNull
    User lockUserByLogin(String login);

    @NotNull
    User unlockUserByLogin(String login);

    @NotNull
    Boolean isLoginExist(String login);

    @Nullable
    Boolean isEmailExist(String email);

}