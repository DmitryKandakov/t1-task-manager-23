package ru.t1.dkandakov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Dmitry Kandakov");
        System.out.println("email: dkandakov@t1-consulting.ru");
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show info about programmer.";
    }

}